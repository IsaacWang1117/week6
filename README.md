# Week6 Mini Project

## Instrumenting a Rust Lambda Function with Logging and Tracing

This guide provides instructions on adding logging and tracing to a Rust Lambda function, integrating AWS X-Ray for tracing, and connecting logs and traces to CloudWatch for monitoring and analysis.


## Setup and Deployment

### Step 1: Clone the Repository

Start by cloning the repository of week2.

### Step 2: Update Function Name

Change the function name to "week6-lambda-function" in the Cargo.toml file.

### Step 3: Development Workflow

For active development, I use the following command to automatically compile the function on code changes. 

```bash
cargo lambda watch
```

### Step 4: Build for Release

```bash
cargo lambda build --release
```

### Step 5: Deploy to AWS Lambda

```bash
cargo lambda deploy
```

### Step 6: Enable AWS X-Ray Tracing and Lambda Insights

After deploying my function, I proceed to enable detailed tracing and monitoring:

Under the Configuration tab of my lambda function, I click on "Monitoring and operations tools". I click edit to enable "AWS X-Ray Active tracing" and "Lambda Insights Enhanced monitoring".

![tracing_enabled](./week6-lambda-function/week6_tracing_enabled.png)


### Step 7: Testing and Monitoring
Finally, I test the function by creating test events and then go to the Monitor tab within the function's AWS Lambda console to view X-Ray traces and logs in CloudWatch.

### Viewing the Results

CloudWatch Logs & Metrics
![CloudWatch and AWS X-Ray traces](./week6-lambda-function/week6_CloudWatch_and_AWS_X-Ray_traces.png)


![tracing details](./week6-lambda-function/week6_trace_details_3.png)


![tracing details](./week6-lambda-function/week6_trace_details_4.png)

![tracing details](./week6-lambda-function/week6_trace_details_5.png)

![tracing details](./week6-lambda-function/week6_trace_details_6.png)


### Conclusion
By following these steps, I successfully added logging and tracing to my Rust Lambda function, integrated AWS X-Ray for detailed tracing insights, and connected logs and traces to CloudWatch for comprehensive monitoring and analysis.



