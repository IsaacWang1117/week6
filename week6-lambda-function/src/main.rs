use tracing_subscriber::filter::{EnvFilter, LevelFilter};use lambda_runtime::{run, service_fn, Error, LambdaEvent};

use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct Operation {
    x: i32,
    y: i32,
}

#[derive(Deserialize)]
struct Request {
    operations: Vec<Operation>,
}

#[derive(Serialize)]
struct Response {
    products: Vec<i32>,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Calculate the product of each operation
    let products = event.payload.operations
        .into_iter()
        .map(|op| op.x * op.y)
        .collect();

    Ok(Response { products })
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
